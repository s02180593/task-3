import sys
import argparse

from graph_generator import graph_generator as gen
from shortest_paths import shortest_paths as path

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Switching functional'
    )
    subparsers = parser.add_subparsers(dest='parser_name')

    parse_path = subparsers.add_parser(
        'shortest-paths',
        help='calculate matrix of shortest paths of directed weighted graph'
    )
    parse_path.add_argument('-i', type=str, help='input file')
    parse_path.add_argument('-o', type=str, help='output file')

    parse_generator = subparsers.add_parser(
        'graph-generator',
        help='generate directed weighted random graph'
    )
    parse_generator.add_argument('-v', type=int, help='num of vertices')
    parse_generator.add_argument('-e', type=int, help='num of edges')
    parse_generator.add_argument('-mw', type=int, help='max weight')
    parse_generator.add_argument('-out', type=str, help='output file')

    args = parser.parse_args(sys.argv[1:])

    if args.parser_name == 'shortest-paths':
        graph = path.read_graph(args.i)
        matrix = path.gen_shortest_path_matrix(graph)
        path.print_matrix(matrix, args.o)
    elif args.parser_name == 'graph-generator':
        if args.mw is None:
            graph = gen.generate_graph(args.v, args.e)
        else:
            graph = gen.generate_graph(args.v, args.e, args.mw)
        gen.print_graph(graph, args.out)
