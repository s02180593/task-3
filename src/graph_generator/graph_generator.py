import networkx as nx
import random as rd
import sys


def generate_graph(v, e, mw=10):
    graph = nx.generators.random_graphs.gnm_random_graph(
        v, e, directed=True
    )
    for (u, v) in graph.edges():
        graph[u][v]['weight'] = rd.randint(0, mw)
    return graph


def print_graph(graph, f_out=sys.stdout):
    if isinstance(f_out, str):
        f_out = open(f_out, 'w')
    else:
        f_out = sys.stdout

    adjacency_matrix = nx.linalg.graphmatrix.adjacency_matrix(graph)
    out = str(adjacency_matrix)
    out = out.replace('[', '').replace(',', '').replace(']', '')

    f_out.write(out)
