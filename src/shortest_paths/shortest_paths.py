import networkx as nx
import sys


def read_graph(f_in=sys.stdin):
    adjacency_dict = {}
    if isinstance(f_in, str):
        f_in = open(f_in, 'r')
    else:
        f_in = sys.stdin

    i = 0
    for line in f_in:
        row = {}
        line = line.strip()
        j = 0
        for memb in line.split(' '):
            if memb.isdigit():
                row[j] = {"weight": int(memb)}
                j += 1
        adjacency_dict[i] = row
        i += 1

    graph = nx.from_dict_of_dicts(adjacency_dict, create_using=nx.DiGraph)
    return graph


def gen_shortest_path_matrix(graph):
    num_nodes = len(graph.nodes())
    matrix = []
    for vert in range(num_nodes):
        path_len = nx.shortest_paths.single_source_dijkstra_path_length(
            graph, vert)
        row = [-1] * num_nodes
        for dest in range(num_nodes):
            row[dest] = path_len[dest]
        matrix.append(row)

    return matrix


def print_matrix(matrix, f_out=sys.stdout):
    if isinstance(f_out, str):
        f_out = open(f_out, 'w')
    else:
        f_out = sys.stdout

    out = str(matrix)
    out = out.replace("], ", '\n').replace(',', '').replace(']', '').replace('[', '')
    f_out.write(out)
